import logo from './logo.svg';
import './App.css';
import Layout from './layout/fullPageLayout';
import Test from "./test/test3";
import Animation from './pages/animation';

function App() {
  return (
    <Animation />
  );
}

export default App;
