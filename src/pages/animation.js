import React, { useState, useEffect } from "react";
import {
  List,
  Avatar,
  Button,
  Modal,
  Input,
  Radio,
  message,
  Layout,
  Typography,
} from "antd";
import Moment from "react-moment";

const { Header, Footer, Sider, Content } = Layout;
const { Text } = Typography;

export default () => {
  const [dataSource, setDataSource] = useState([]);
  const [addVisible, setAddVisible] = useState(false);
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [isPublished, setIsPublished] = useState();
  const [modalStatus, setModalStatus] = useState("");
  const [editId, setEditId] = useState();

  const url = "http://3.135.201.25:5001/api/goosemations/";
  // const url = 'http://localhost:3000/api/animations/';
  useEffect(() => {
    getAnimationSource();
  }, []);

  const onModalVisible = () => {
    setAddVisible(true);
  };

  const handleOk = () => {
    setName("");
    setType("");
    setIsPublished();
    setAddVisible(false);
    const method = modalStatus === "Add" ? "POST" : "PUT";
    if (method === "POST") {
      getApi(modalStatus, method);
    } else {
      getApi(modalStatus, method, editId);
    }
  };

  const handleCancel = () => {
    setName("");
    setType("");
    setIsPublished();
    setAddVisible(false);
  };

  const getApiEditField = async (id) => {
    let result = 0;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const data = await fetch(url + id, config)
      .then((res) => res.json())
      .then((response) => {
        console.log("res", response);
        result = response;
      });

    setName(result.name);
    setType(result.type);
    setIsPublished(result.isPublished);
  };

  const getApi = async (value, method, id) => {

    let result = 0;

    const body = {
      name: name,
      type: type,
      isPublished: isPublished,
    };
    const config = {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    };

    if (value === "Add") {
      const data = await fetch(url, config)
        .then((res) => res.json())
        .then((response) => {
          console.log("Add res", response);
          result = response;
        });

      setTimeout(() => {
        message.success(`${modalStatus} ${result.data.name} Success`);
      }, 700);

      getAnimationSource();
    } else if (value === "Update") {
      const data = await fetch(url + id, config)
        .then((res) => res.json())
        .then((response) => {
          result = response;
        });

      setTimeout(() => {
        message.success(`${result.message}`);
      }, 700);

      getAnimationSource();
    } else if (value === "Delete") {
      console.log("This delete Id: ", id);
      const data = await fetch(url + `${id}`, config)
        .then((res) => res.json())
        .then((response) => {
          console.log("Delete Data: ", response);

          result = response;
        });

      setTimeout(() => {
        message.success(`${result.message}`);
      }, 700);

      getAnimationSource();
    } else {
      const data = await fetch(url)
        .then((res) => res.json())
        .then((response) => {
          result = response;
        });
    }

    return result;
  };

  const getAnimationSource = async () => {
    const data = await getApi("", "GET");

    setDataSource(data);
  };

  console.log("DataSource", dataSource);

  const onClickDelete = (deleteId) => {
    console.log("onClickDelete", deleteId);
    getApi("Delete", "DELETE", deleteId);
  };

  const onModalStatus = (value, editId) => {
    setModalStatus(value);
    setEditId(editId);
    getApiEditField(editId);
    onModalVisible();
  };

  const onNameInputChange = (e) => {
    const value = e.target.value;
    setName(value);
  };
  const onTypeInputChange = (e) => {
    const value = e.target.value;
    setType(value);
  };

  const onIspublishedChange = (e) => {
    const value = e.target.value;
    setIsPublished(value);
  };

  return (
    <div>
      <Layout>
        <Header
          style={{
            background: "#fafafa",
            padding: 0,
            boxShadow: "rgb(133, 133, 133, 0.1)",
            textAlign: "center",
          }}
        >
          <Text style={{ fontSize: "18px" }} strong>
            The Animation Project
          </Text>
        </Header>
        <Content>
          <List
            itemLayout="horizontal"
            style={{margin: '2%'}}
            dataSource={dataSource ? dataSource : null}
            renderItem={(item) => (
              <List.Item
                actions={[
                  <a onClick={() => onModalStatus("Update", item._id)}>Edit</a>,

                  <a
                    style={{ color: "red" }}
                    onClick={() => onClickDelete(item._id)}
                  >
                    Delete
                  </a>,
                  <p key="list-loadmore-edit"></p>,
                ]}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      size="large"
                      src="https://cdn71.picsart.com/186528543003202.gif?to=crop&r=256"
                    />
                  }
                  title={<a href="https://ant.design">{item.name}</a>}
                  description={
                    <div>
                      type: {item.type}
                      <br /> isPublished: {JSON.stringify(item.isPublished)}
                      <br /> Create: {<Moment local>{item.createdAt}</Moment>}
                      <br /> Update: {<Moment local>{item.updatedAt}</Moment>}
                    </div>
                  }
                />
              </List.Item>
            )}
          />
          <Button
            shape = 'round'
            style={{ 
              backgroundColor: 'rgb(2, 192, 118)', 
              color: 'white',
              width: "8%",
              cursor: 'pointer',
              height: '40px',
              float: 'right',
              margin: '2%'

            }}
            onClick={() => onModalStatus("Add", "")}

          >
            Add
          </Button>

          <Modal
            title={`${modalStatus}`}
            visible={addVisible}
            onOk={handleOk}
            okText={modalStatus == "Add" ? "Add" : "Update"}
            onCancel={handleCancel}
          >
            <Input
              style={{ marginBottom: "1%", height: "40px" }}
              placeholder="Name"
              value={name}
              onChange={onNameInputChange}
            />
            <Input
              style={{ height: "40px" }}
              placeholder="Type"
              value={type}
              onChange={onTypeInputChange}
            />
            <h4 style={{ marginRight: "1%", marginTop: "2%" }}> Published </h4>
            <Radio.Group onChange={onIspublishedChange} value={isPublished}>
              <Radio value={true}>True</Radio>
              <Radio value={false}>False</Radio>
            </Radio.Group>
          </Modal>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          <Text type="secondary">
            The Animation Studio ©2021 Created by Praew
          </Text>
        </Footer>
      </Layout>
    </div>
  );
};
